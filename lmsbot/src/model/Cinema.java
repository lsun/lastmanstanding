package model;

import java.util.Set;

public class Cinema {
	private int id;
	private String name;
	private String address;
	private String phone;
	private double longitude;
	private double latitude;
	private float rating;
	private Set<CinemaMovie> cinemaMovie;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public Set<CinemaMovie> getCinemaMovie() {
		return cinemaMovie;
	}
	public void setCinemaMovie(Set<CinemaMovie> cinemaMovie) {
		this.cinemaMovie = cinemaMovie;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public Set<CinemaMovie> getMovies() {
		return cinemaMovie;
	}
	public void setMovies(Set<CinemaMovie> cinemaMovie) {
		this.cinemaMovie = cinemaMovie;
	}
}
