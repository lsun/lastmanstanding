package model;

public class Movie {
	private int id;
	private String name;
	private String languages;
	private String outline;
	private String ageRating;
	private int durationMins;
	private String genres;
	private float imdbRating;
	private String director;
	private String thumbnail;
	private String casts;
	private long releaseDate;
	private String distributor;
	private float rating;
	
	public String getOutline() {
		return outline;
	}
	public void setOutline(String outline) {
		this.outline = outline;
	}
	public String getAgeRating() {
		return ageRating;
	}
	public void setAgeRating(String ageRating) {
		this.ageRating = ageRating;
	}
	public int getDurationMins() {
		return durationMins;
	}
	public void setDurationMins(int durationMins) {
		this.durationMins = durationMins;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public float getImdbRating() {
		return imdbRating;
	}
	public void setImdbRating(float imdbRating) {
		this.imdbRating = imdbRating;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getCasts() {
		return casts;
	}
	public void setCasts(String casts) {
		this.casts = casts;
	}
	public long getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getLanguages() {
		return languages;
	}
	public void setLanguages(String languages) {
		this.languages = languages;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == this) {
			return true;
		}
		
		if(!(other instanceof Movie)) {
			return false;
		}
		
	    Movie otherMovie = (Movie)other;
	    return otherMovie.getName().toLowerCase().equals(this.getName().toLowerCase());
	}
	
	@Override
	public int hashCode() {
	    int result = 17;
	    result = 31 * result + this.getName().toLowerCase().hashCode();
	    return result;
	}
}
