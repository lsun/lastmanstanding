package model;

import java.util.Date;

public class CinemaMovie {
	private CinemaMovieMapping id;
	private long showTime;
	
	public CinemaMovieMapping getId() {
		return id;
	}
	public void setId(CinemaMovieMapping id) {
		this.id = id;
	}
	public long getShowTime() {
		return showTime;
	}
	public void setShowTime(long showTime) {
		this.showTime = showTime;
	}
}
