package model;

import java.io.Serializable;

public class CinemaMovieMapping implements Serializable {
	private Cinema cinema;
	private Movie movie;
	
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
}
