import java.sql.PreparedStatement;
import java.util.List;

import initializer.CinemaInitializer;
import initializer.CityInitializer;
import initializer.CountryInitializer;
import initializer.MovieInitializer;
import model.CinemaMovie;
import tool.DatabaseManager;
import tool.provider.PopcornProvider;

public class Starter {
	public static void main(String[] args) {
		Starter starter = new Starter();
		starter.start();
		
//		PopcornProvider provider = new PopcornProvider();
//		provider.processCinemaes();
	}
	
	public void start() {
		System.out.println("Starting LSM...");
		initialize();
		update();
	}
	
	private void initialize() {
		(new CountryInitializer()).initialize();
		(new CityInitializer()).initialize();
	}
	
	private void update() {
		PopcornProvider provider = new PopcornProvider();
		provider.processCinemaes();
		
		// update cinemaes
		CinemaInitializer cinemaInitializer = new CinemaInitializer();
		cinemaInitializer.initialize(provider.getCinemaes());
		
		// update movies
		MovieInitializer movieInitializer = new MovieInitializer();
		movieInitializer.initialize(provider.getMovies());
		
		// update cinema movie	
//		updateCinemaMovies(provider.getCinemaMovies());
	}
	
	private void updateCinemaMovies(List<CinemaMovie> cinemaMovies) {
		DatabaseManager manager = new DatabaseManager();
	    
		System.out.println("Length : " + cinemaMovies.size());
	    for(int i = 0, len = cinemaMovies.size(); i < len; ++i) {
	    	System.out.println("Current : " + i + ", Length : " + len);
	    	CinemaMovie cinemaMovie = cinemaMovies.get(i);
	    	try {
	    		String sql = "SELECT COUNT(1) FROM cinema_movie WHERE cinema_id = ? AND movie_id = ? AND show_time = ?";
	    		PreparedStatement countSt = manager.createPreparedStatement(sql);
	    		countSt.setInt(1, cinemaMovie.getId().getCinema().getId());
	    		countSt.setInt(2, cinemaMovie.getId().getMovie().getId());
	    		countSt.setLong(3, cinemaMovie.getShowTime());
	    		int count = manager.count(countSt);
	    		
	    		if(count == 0) {
				    PreparedStatement st = manager.createPreparedStatement("INSERT INTO cinema_movie (cinema_id, movie_id, show_time) VALUES (?, ?, ?)");
				    st.setInt(1,  cinemaMovie.getId().getCinema().getId());
				    st.setInt(2,  cinemaMovie.getId().getMovie().getId());
				    st.setLong(3, cinemaMovie.getShowTime());
				    manager.update(st);
	    		} else {
	    			System.out.println("Existing cinema movie...");
	    		}
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	    	}
	    }
	    
	    manager.close();
	    System.out.println("Done updating cinema movie");
	}
}
