package tool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManager {
	private final static String URL      = "jdbc:postgresql://localhost:5432/lsm";
	private final static String USER     = "lsm";
	private final static String PASSWORD = "lsm";
	
//	private final static String URL      = "jdbc:postgresql://api.rella.im:5432/lms";
//	private final static String USER     = "lms";
//	private final static String PASSWORD = "lms";
	
	private Connection connection = null;
	
	public DatabaseManager() {
		this(URL, USER, PASSWORD);
	}
	
	public DatabaseManager(String url, String user, String password) {
	    try {
	    	connection = DriverManager.getConnection(url, user, password);
	        System.out.println("Connected to the PostgreSQL server successfully.");
	    } catch (SQLException e) {
	        System.out.println(e.getMessage());
	    }
	}
	
	public PreparedStatement createPreparedStatement(String query) {
		try {
			return connection.prepareStatement(query);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public int count(PreparedStatement stmt) {
		int count = 0;
		try {
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return count;
	}
	
	public int update(PreparedStatement stmt) {
		int updatedCount = 0;
		try {
			updatedCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return updatedCount;
	}
	
	public void close() {
		try {
			if(connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
