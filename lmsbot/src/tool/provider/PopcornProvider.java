package tool.provider;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.Cinema;
import model.CinemaMovie;
import model.CinemaMovieMapping;
import model.Movie;
import tool.Spider;

public class PopcornProvider extends Provider {
	private static String CINEMA_LIST_URL = "https://www.popcorn.app/sg/cinemas";
	private static String MOVIE_SHOWTIME_START_TOKEN = "allShowtimes: ";
	private static String MOVIE_SHOWTIME_END_TOKEN   = "flagCinema:";
	
	public void processCinemaes() {
		Document doc = Spider.visit(CINEMA_LIST_URL);
		this.parseCinemaes(doc);
	}
	
	public void parseCinemaes(Document doc) {
		Elements elements = doc.select(".list-group-item");
		Iterator iterator = elements.iterator();
		while(iterator.hasNext()) {
			try {
				Element elem = (Element)iterator.next();
				Elements anchors = elem.getElementsByTag("a");
				Element anchor = anchors.first();
				
				Cinema cinema = new Cinema();
				cinema.setName(anchor.text());
				
				
				Document cinemaDoc = Spider.visit(anchor.attr("href"));
				parseLocation(cinema, cinemaDoc);
				try {
					parseMovies(cinema, cinemaDoc);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				this.getCinemaes().add(cinema);
			} catch (Exception ex) {
				System.out.println(ex.getSuppressed());
			}
		}
	}
	
	private Cinema parseLocation(Cinema cinema, Document doc) {
		Elements elements = doc.select(".address-bar");
		String location = elements.first().getElementsByTag("p").first().text();
		String googleMapLink = elements.first().selectFirst("div > a").attr("href");
		cinema.setAddress(location);
		
		// Get longitude and latitude
		String[] tokens = googleMapLink.split("=");
		tokens = tokens[1].split(",");
		if(tokens.length >= 2) {
			cinema.setLatitude(Float.parseFloat(tokens[0]));
			cinema.setLongitude(Float.parseFloat(tokens[1]));
		}
		
		String phone = elements.first().selectFirst(".fa-phone").siblingNodes().get(0).toString().trim();
		phone = phone.replaceAll(" ", "");
		cinema.setPhone(phone);
//		System.out.println("Location : " + location);
//		System.out.println("Google Map Link : " + googleMapLink);
//		System.out.println("Latiutude : " + cinema.getLatitude());
//		System.out.println("Longitude : " + cinema.getLongitude());
//		System.out.println("Phone : " + cinema.getPhone());
		return cinema;
	}
	
	private void parseMovies(Cinema cinema, Document doc) {
		String sourceCode = doc.html();
		String movies = sourceCode.substring(sourceCode.indexOf(MOVIE_SHOWTIME_START_TOKEN) + MOVIE_SHOWTIME_START_TOKEN.length(), sourceCode.indexOf(MOVIE_SHOWTIME_END_TOKEN));
		movies = movies.trim().substring(0, movies.lastIndexOf(","));
		
		HashMap<String, Object> showTimeMap = createHashMapFromJsonString(movies);
		Set keys = showTimeMap.keySet();
		Iterator keyIterator = keys.iterator();
		while(keyIterator.hasNext()) {
			String key = (String)keyIterator.next();
//			System.out.println("Date string " + key);
			List movieList = (List)showTimeMap.get(key);
			for(int i = 0, len = movieList.size(); i < len; ++i) {
				// This Map contains the actual movie information
				HashMap singleMovieMap = (HashMap)movieList.get(i);
//				System.out.println(singleMovieMap.get("MovieName").toString());
				
				Movie movie = new Movie();
				movie.setName(singleMovieMap.get("MovieName").toString());
				if(!this.getMovies().contains(movie)) {
					movie.setLanguages(singleMovieMap.get("Language").toString());
					movie.setThumbnail(singleMovieMap.get("ListingImage").toString());
					movie.setGenres(singleMovieMap.get("Genre").toString());
					
					String movieUrl = singleMovieMap.get("MovieUrl").toString();
					Document movieDoc = Spider.visit(movieUrl);
					parseMovie(movie, movieDoc);
					
					// TODO: Rating is currently faked. Will pull from IMDB later
					movie.setImdbRating((float)(ThreadLocalRandom.current().nextInt(3, 9)*1.0));
					
					// TODO: Get Rating
					this.getMovies().add(movie);
				} else {
					movie = this.getMovies().get(this.getMovies().indexOf(movie));
				}				
				
				// For showtime
				HashMap cinemaMap = (HashMap)singleMovieMap.get("Cinemas");
				Iterator iter = cinemaMap.values().iterator();
				while(iter.hasNext()) {
					List showTimeList = (List)iter.next();
					for(int j = 0, jLen = showTimeList.size(); j < jLen; ++j) {
						HashMap singleShowTimeMap = (HashMap)showTimeList.get(j);
//						System.out.println(singleShowTimeMap.get("ShowTime").toString());
						
						String computedShowTime = key + " " + singleShowTimeMap.get("ShowTime").toString();
//						System.out.println(computedShowTime);
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd KK:mma");
						try {
							Date date = dateFormat.parse(computedShowTime);
							
							CinemaMovieMapping mapping = new CinemaMovieMapping();
							mapping.setCinema(cinema);
							mapping.setMovie(movie);
							
							CinemaMovie cinemaMovie = new CinemaMovie();
							cinemaMovie.setId(mapping);
							cinemaMovie.setShowTime(date.getTime()/1000L);
							
							this.getCinemaMovies().add(cinemaMovie);
//							System.out.println(date.toString());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	private void parseMovie(Movie movie, Document doc) {
		String director = parseDirector(doc);
		movie.setDirector(director);
		
		String casts = parseCasts(doc);
//		System.out.println("Casts: " + casts);
		movie.setCasts(casts);
		
		long releaseDate = parseReleaseDate(doc);
//		System.out.println("Release Date: " + releaseDate);
		movie.setReleaseDate(releaseDate);
		
		int duration = parseDuration(doc);
//		System.out.println("Duration: " + duration);
		movie.setDurationMins(duration);
		
		String distributor = parseDistributor(doc);
//		System.out.println("Distributor: " + distributor);
		movie.setDistributor(distributor);
		
		String outline = parseDescription(doc);
//		System.out.println("Outline: " + outline);
		movie.setOutline(outline);
	}
	
	private String parseDirector(Document doc) {
		return doc.selectFirst("div[itemprop=director]").selectFirst("span[itemprop=name]").text();
	}
	
	private String parseCasts(Document doc) {
		Elements actors = doc.select("span[itemprop=actors]");
		StringBuilder actorStrBuilder = new StringBuilder();
		for(int i = 0, len = actors.size(); i < len; ++i) {
			actorStrBuilder.append(actors.get(i).selectFirst("span[itemprop=name]").text());
			if(i != (len - 1)) {
				actorStrBuilder.append(", ");
			}
		}
		return actorStrBuilder.toString();
	}
	
	private long parseReleaseDate(Document doc) {
		String dateStr = doc.selectFirst("span[itemprop=scheduledTime]").text();
		DateFormat dateFormat = new SimpleDateFormat("dd MMMMMM, yyyy");
		try {
			Date date = dateFormat.parse(dateStr);
			return date.getTime()/1000L;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private int parseDuration(Document doc) {
		try {
			return Integer.parseInt(doc.selectFirst("td[itemprop=duration]").selectFirst("span[itemprop=name]").text().split(" ")[0]);
		} catch(Exception ex) {
			return 0;
		}
	}
	
	private String parseDistributor(Document doc) {
		try {
			return doc.selectFirst("td[itemprop=productionCompany]").selectFirst("span[itemprop=name]").text().trim();
		} catch(Exception ex) {
			return "";
		}
	}
	
	private String parseDescription(Document doc) {
		try {
			return doc.selectFirst("p[itemprop=description]").text().trim();
		} catch(Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}
	
    private HashMap<String, Object> createHashMapFromJsonString(String json) {
    	JsonParser parser = new JsonParser();
        JsonObject object = (JsonObject) parser.parse(json);
        Set<Map.Entry<String, JsonElement>> set = object.entrySet();
        Iterator<Map.Entry<String, JsonElement>> iterator = set.iterator();
        HashMap<String, Object> map = new HashMap<String, Object>();

        while (iterator.hasNext()) {
            Map.Entry<String, JsonElement> entry = iterator.next();
            String key = entry.getKey();
            JsonElement value = entry.getValue();

            if (null != value) {
                if (!value.isJsonPrimitive()) {
                    if (value.isJsonObject()) {
                        map.put(key, createHashMapFromJsonString(value.toString()));
                    } else if (value.isJsonArray() && value.toString().contains(":")) {
                        List<HashMap<String, Object>> list = new ArrayList<>();
                        JsonArray array = value.getAsJsonArray();
                        if (null != array) {
                            for (JsonElement element : array) {
                                list.add(createHashMapFromJsonString(element.toString()));
                            }
                            map.put(key, list);
                        }
                    } else if (value.isJsonArray() && !value.toString().contains(":")) {
                        map.put(key, value.getAsJsonArray());
                    }
                } else {
                    map.put(key, value.getAsString());
                }
            }
        }
        return map;
    }
}
