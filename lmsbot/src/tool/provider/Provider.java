package tool.provider;

import java.util.ArrayList;
import java.util.List;
import model.Cinema;
import model.CinemaMovie;
import model.Movie;

public abstract class Provider {
	private String cinemaUrl = "";
	private List<Cinema> cinemaes;
	private List<Movie> movies;
	private List<CinemaMovie> cinemaMovies;
	
	public Provider() {
		this.cinemaes = new ArrayList<Cinema>();
		this.movies = new ArrayList<Movie>();
		this.cinemaMovies = new ArrayList<CinemaMovie>();
	}
	
	public Provider(String cinemaUrl) {
		this();
		this.cinemaUrl = cinemaUrl;
	}

	public String getCinemaUrl() {
		return cinemaUrl;
	}

	public void setCinemaUrl(String cinemaUrl) {
		this.cinemaUrl = cinemaUrl;
	}

	public List<Cinema> getCinemaes() {
		return cinemaes;
	}

	public void setCinemaes(List<Cinema> cinemaes) {
		this.cinemaes = cinemaes;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public List<CinemaMovie> getCinemaMovies() {
		return cinemaMovies;
	}

	public void setCinemaMovies(List<CinemaMovie> cinemaMovies) {
		this.cinemaMovies = cinemaMovies;
	}
}
