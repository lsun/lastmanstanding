package initializer;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import model.City;
import model.Country;

public class CityInitializer implements Initializer{
	private static List<City> cityList = new ArrayList<City>();
	
	public void initialize() {
		System.out.println("Initializing cities...");
		CityInitializer.populateCities();
	}
	
	public static void populateCities() {
		buildCityList(cityList);
		
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		

		for(int i = 0, len = cityList.size(); i < len; ++i) {
			City city = cityList.get(i);
			try {
				System.out.println("Adding city " + city.getName());
				if(null == session.get(City.class, city.getId())){
					Transaction t = session.beginTransaction();
					session.save(city);
					t.commit();
				}
			} catch (Exception ex) {
				System.out.println("Failed to save city : " + city.getName());
				ex.printStackTrace();
			}
		}

		System.out.println("successfully saved");
		factory.close();
		session.close();
	}
	
	private static void buildCityList(List cityList) {
		Country country = new Country();
		country.setId(1);
		
		cityList.add(createCity(1, "Singapore", country));
	}
	
	private static City createCity(int id, String name, Country country) {
		City city = new City();
		city.setId(id);
		city.setName(name);
		city.setCountry(country);
		
		return city;
	}
}
