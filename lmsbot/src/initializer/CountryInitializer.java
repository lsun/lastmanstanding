package initializer;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import model.Country;

public class CountryInitializer implements Initializer{
	private static List<Country> countryList = new ArrayList<Country>();
	
	public void initialize() {
		System.out.println("Initializing countries...");
		CountryInitializer.populateCountries();
	}
	
	public static void populateCountries() {
		buildCountryList(countryList);
		
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		

		for(int i = 0, len = countryList.size(); i < len; ++i) {
			Country country = countryList.get(i);
			try {
				System.out.println("Adding copuntry " + country.getName());
				if(null == session.get(Country.class, country.getId())){
					Transaction t = session.beginTransaction();
					session.save(country);
					t.commit();
				}
			} catch (Exception ex) {
				System.out.println("Failed to save country : " + country.getName());
				ex.printStackTrace();
			}
		}

		System.out.println("successfully saved");
		factory.close();
		session.close();
	}
	
	private static void buildCountryList(List countryList) {
		countryList.add(createCountry(1, "Singapore", "SG"));
		countryList.add(createCountry(2, "Malaysia",  "MA"));
	}
	
	private static Country createCountry(int id, String name, String shortCode) {
		Country country = new Country();
		country.setId(id);
		country.setName(name);
		country.setShortCode(shortCode);
		
		return country;
	}
}
