package initializer;

public interface Initializer {
	public void initialize();
}
