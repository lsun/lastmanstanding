package initializer;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Restrictions;

import model.Movie;

public class MovieInitializer implements Initializer {
	private static List<Movie> list = new ArrayList<Movie>();
	
	public void initialize() {
		System.out.println("Initializing movies...");
//		buildCinemaList(list);
		MovieInitializer.populate(list);
	}
	
	public void initialize(List<Movie> list) {
		System.out.println("Initializing movies...");
		MovieInitializer.populate(list);
	}
	
	private static void populate(List<Movie> list) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
//		Transaction t = session.beginTransaction();

		for(int i = 0, len = list.size(); i < len; ++i) {
			Movie movie = list.get(i);
			try {
				System.out.println("Adding movie " + movie.getName());
				Movie existingMovie = findByName(session, movie.getName());
				if(existingMovie == null) {
					System.out.println("Not existing " + movie.getName());
					Transaction t = session.beginTransaction();
					Integer id = (Integer)session.save(movie);
					movie.setId(id);
					t.commit();
				} else {
					System.out.println("Existing " + movie.getName());
					existingMovie.setLanguages(movie.getLanguages());
					existingMovie.setThumbnail(movie.getThumbnail());
					existingMovie.setGenres(movie.getGenres());
					existingMovie.setDirector(movie.getDirector());
					existingMovie.setCasts(movie.getCasts());
					existingMovie.setDistributor(movie.getDistributor());
					existingMovie.setDurationMins(movie.getDurationMins());
					existingMovie.setReleaseDate(movie.getReleaseDate());
//					existingMovie.setOutline(movie.getOutline());
					existingMovie.setImdbRating(movie.getImdbRating());
					
					movie.setId(existingMovie.getId());
					Transaction t = session.beginTransaction();
					session.update(existingMovie);
					t.commit();
				}
			} catch (Exception ex) {
				System.out.println("Failed to save cinema : " + movie.getName());
				ex.printStackTrace();
			}
		}

		System.out.println("successfully saved");
		factory.close();
		session.close();
	}
	
//	private static void buildCinemaList(List cinemaList) {
//		City city = new City();
//		city.setId(1);
//		
//		cinemaList.add(createCinema(1, "Tampines", 1.1, 1.2, city));
//	}
//	
//	private static Cinema createCinema(int id, String name, double latitude, double longitude, City city) {
//		Cinema cinema = new Cinema();
//		cinema.setId(id);
//		cinema.setName(name);
//		cinema.setLatitude(latitude);
//		cinema.setLongitude(longitude);
//		cinema.setCity(city);
//		
//		return cinema;
//	}
	
	private static Movie findByName(Session session, String name) {
		Criteria criteria = session.createCriteria(Movie.class);
		criteria.add(Restrictions.eq("name", name));
		Movie movie = (Movie) criteria.uniqueResult();
		return movie;
	}
}
