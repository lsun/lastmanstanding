package initializer;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import model.Cinema;
import model.CinemaMovie;
import model.City;
import model.Country;

public class CinemaInitializer implements Initializer {
	private static List<Cinema> cinemaList = new ArrayList<Cinema>();
	
	public void initialize() {
		System.out.println("Initializing cinemaes...");
		buildCinemaList(cinemaList);
		CinemaInitializer.populateCinemaes(cinemaList);
	}
	
	public void initialize(List<Cinema> cinemaList) {
		System.out.println("Initializing cinemaes...");
		CinemaInitializer.populateCinemaes(cinemaList);
	}
	
	public void initialize(List<Cinema> cinemaList, List<CinemaMovie> cinemaMovies) {
		System.out.println("Initializing cinemaes...");
		CinemaInitializer.populateCinemaes(cinemaList, cinemaMovies);
	}
	
	private static void populateCinemaes(List<Cinema> cinemaList) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		for(int i = 0, len = cinemaList.size(); i < len; ++i) {
			Cinema cinema = cinemaList.get(i);
			try {
				System.out.println("Adding cinema " + cinema.getName());
				Cinema existingCinema = findByName(session, cinema.getName());
				if(existingCinema == null) {
					System.out.println("Not existing " + cinema.getName());
					Transaction t = session.beginTransaction();
					Integer id = (Integer)session.save(cinema);
					cinema.setId(id);
					t.commit();
				} 
				else {
					System.out.println("Existing " + cinema.getName());
					existingCinema.setLatitude(cinema.getLatitude());
					existingCinema.setLongitude(cinema.getLongitude());
					existingCinema.setAddress(cinema.getAddress());
					existingCinema.setPhone(cinema.getPhone());
					existingCinema.setRating(cinema.getRating());
					cinema.setId(existingCinema.getId());
					session.update(existingCinema);
				}
			} catch (Exception ex) {
				System.out.println("Failed to save cinema : " + cinema.getName());
				ex.printStackTrace();
			}
		}

		System.out.println("successfully saved");
		factory.close();
		session.close();
	}
	
	private static void populateCinemaes(List<Cinema> cinemaList, List<CinemaMovie> cinemaMovies) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		for(int i = 0, len = cinemaList.size(); i < len; ++i) {
			Cinema cinema = cinemaList.get(i);
			try {
				System.out.println("Adding cinema " + cinema.getName());
				Cinema existingCinema = findByName(session, cinema.getName());
				if(existingCinema == null) {
					System.out.println("Not existing " + cinema.getName());
					Transaction t = session.beginTransaction();
					Integer id = (Integer)session.save(cinema);
					cinema.setId(id);
					t.commit();
				} else {
					System.out.println("Existing " + cinema.getName());
					existingCinema.setLatitude(cinema.getLatitude());
					existingCinema.setLongitude(cinema.getLongitude());
					existingCinema.setAddress(cinema.getAddress());
					cinema.setId(existingCinema.getId());
					Transaction t = session.beginTransaction();
					session.update(existingCinema);
					t.commit();
				}
			} catch (Exception ex) {
				System.out.println("Failed to save cinema : " + cinema.getName());
				ex.printStackTrace();
			}
		}

		System.out.println("successfully saved");
		factory.close();
		session.close();
	}
	
	private static void buildCinemaList(List cinemaList) {
		City city = new City();
		city.setId(1);
		
		cinemaList.add(createCinema(1, "Tampines", 1.1, 1.2, city));
	}
	
	private static Cinema createCinema(int id, String name, double latitude, double longitude, City city) {
		Cinema cinema = new Cinema();
		cinema.setId(id);
		cinema.setName(name);
		cinema.setLatitude(latitude);
		cinema.setLongitude(longitude);
		
		return cinema;
	}
	
	private static Cinema findByName(Session session, String name) {
		Criteria criteria = session.createCriteria(Cinema.class);
		criteria.add(Restrictions.eq("name", name));
		Cinema cinema = (Cinema) criteria.uniqueResult();
		return cinema;
	}
}
