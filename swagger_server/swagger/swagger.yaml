---
swagger: "2.0"
info:
  description: "Movies and Cinemas you like."
  version: "1.0.0"
  title: "Movie Widgethon"
  contact:
    name: "Last Man Standing"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "api.rella.im"
basePath: "/v1"
schemes:
- "https"
consumes:
- "application/json"
produces:
- "application/json"
paths:
  /cinemas:
    get:
      operationId: "cinemas_get"
      parameters:
      - name: "country"
        in: "query"
        required: true
        type: "string"
        enum:
        - "SG"
        - "ID"
        - "MY"
        - "PH"
        - "VN"
        - "TH"
        - "KH"
        - "MM"
      - name: "city"
        in: "query"
        required: true
        type: "string"
      - name: "latitude"
        in: "query"
        required: true
        type: "number"
        format: "double"
      - name: "longitude"
        in: "query"
        required: true
        type: "number"
        format: "double"
      responses:
        200:
          description: "list of cinemas"
          schema:
            $ref: "#/definitions/inline_response_200"
        400:
          description: "Bad Request"
        500:
          description: "Internal Server Error"
      x-swagger-router-controller: "swagger_server.controllers.default_controller"
  /cinemas/{id}:
    get:
      operationId: "cinemas_id_get"
      parameters:
      - name: "id"
        in: "path"
        required: true
        type: "integer"
      responses:
        200:
          description: "list of movies and cinema info"
          schema:
            $ref: "#/definitions/CinemaDetail"
        400:
          description: "Bad Request"
        500:
          description: "Internal Server Error"
      x-swagger-router-controller: "swagger_server.controllers.default_controller"
definitions:
  CinemaSummary:
    type: "object"
    properties:
      id:
        type: "integer"
      name:
        type: "string"
      thumbnail:
        type: "string"
      latitude:
        type: "number"
      longitude:
        type: "number"
      phone:
        type: "string"
      address:
        type: "string"
      rating:
        type: "number"
      distance_km:
        type: "number"
    example:
      thumbnail: "thumbnail"
      address: "address"
      distance_km: 5.63737665663332876420099637471139430999755859375
      phone: "phone"
      latitude: 6.02745618307040320615897144307382404804229736328125
      name: "name"
      rating: 5.962133916683182377482808078639209270477294921875
      id: 0
      longitude: 1.46581298050294517310021547018550336360931396484375
  Movie:
    type: "object"
    properties:
      id:
        type: "integer"
      name:
        type: "string"
      language:
        type: "string"
      outline:
        type: "string"
      age_rating:
        type: "string"
      duration_mins:
        type: "integer"
      genres:
        type: "array"
        items:
          type: "string"
      imdb_rating:
        type: "number"
      director:
        type: "string"
      thumbnail:
        type: "string"
      cast:
        type: "array"
        items:
          type: "string"
      release_date:
        type: "integer"
      distributor:
        type: "string"
      show_times:
        type: "array"
        items:
          type: "integer"
    example:
      thumbnail: "thumbnail"
      age_rating: "age_rating"
      director: "director"
      show_times:
      - 2
      - 2
      language: "language"
      distributor: "distributor"
      cast:
      - "cast"
      - "cast"
      outline: "outline"
      duration_mins: 7
      release_date: 3
      genres:
      - "genres"
      - "genres"
      name: "name"
      id: 2
      imdb_rating: 9.301444243932575517419536481611430644989013671875
  CinemaDetail:
    type: "object"
    properties:
      id:
        type: "integer"
      name:
        type: "string"
      thumbnail:
        type: "string"
      latitude:
        type: "number"
      longitude:
        type: "number"
      phone:
        type: "string"
      address:
        type: "string"
      rating:
        type: "number"
      distance_km:
        type: "number"
      movies:
        type: "array"
        items:
          $ref: "#/definitions/Movie"
    example:
      movies:
      - thumbnail: "thumbnail"
        age_rating: "age_rating"
        director: "director"
        show_times:
        - 2
        - 2
        language: "language"
        distributor: "distributor"
        cast:
        - "cast"
        - "cast"
        outline: "outline"
        duration_mins: 7
        release_date: 3
        genres:
        - "genres"
        - "genres"
        name: "name"
        id: 2
        imdb_rating: 9.301444243932575517419536481611430644989013671875
      - thumbnail: "thumbnail"
        age_rating: "age_rating"
        director: "director"
        show_times:
        - 2
        - 2
        language: "language"
        distributor: "distributor"
        cast:
        - "cast"
        - "cast"
        outline: "outline"
        duration_mins: 7
        release_date: 3
        genres:
        - "genres"
        - "genres"
        name: "name"
        id: 2
        imdb_rating: 9.301444243932575517419536481611430644989013671875
      thumbnail: "thumbnail"
      address: "address"
      distance_km: 5.63737665663332876420099637471139430999755859375
      phone: "phone"
      latitude: 6.02745618307040320615897144307382404804229736328125
      name: "name"
      rating: 5.962133916683182377482808078639209270477294921875
      id: 0
      longitude: 1.46581298050294517310021547018550336360931396484375
  inline_response_200:
    required:
    - "cinemas"
    properties:
      cinemas:
        type: "array"
        items:
          $ref: "#/definitions/CinemaSummary"
    example:
      cinemas:
      - thumbnail: "thumbnail"
        address: "address"
        distance_km: 5.63737665663332876420099637471139430999755859375
        phone: "phone"
        latitude: 6.02745618307040320615897144307382404804229736328125
        name: "name"
        rating: 5.962133916683182377482808078639209270477294921875
        id: 0
        longitude: 1.46581298050294517310021547018550336360931396484375
      - thumbnail: "thumbnail"
        address: "address"
        distance_km: 5.63737665663332876420099637471139430999755859375
        phone: "phone"
        latitude: 6.02745618307040320615897144307382404804229736328125
        name: "name"
        rating: 5.962133916683182377482808078639209270477294921875
        id: 0
        longitude: 1.46581298050294517310021547018550336360931396484375
responses:
  400:
    description: "Bad Request"
  500:
    description: "Internal Server Error"
