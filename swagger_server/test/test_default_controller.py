# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.cinema_detail import CinemaDetail  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_cinemas_get(self):
        """Test case for cinemas_get

        
        """
        query_string = [('country', 'SG'),
                        ('city', 'city_example'),
                        ('latitude', 1.2),
                        ('longitude', 1.2)]
        response = self.client.open(
            '/v1/cinemas',
            method='GET',
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_cinemas_id_get(self):
        """Test case for cinemas_id_get

        
        """
        response = self.client.open(
            '/v1/cinemas/{id}'.format(id=56),
            method='GET',
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
