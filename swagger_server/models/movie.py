# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class Movie(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(self, id: int=None, name: str=None, language: str=None, outline: str=None, age_rating: str=None, duration_mins: int=None, genres: List[str]=None, imdb_rating: float=None, director: str=None, thumbnail: str=None, cast: List[str]=None, release_date: int=None, distributor: str=None, show_times: List[int]=None):  # noqa: E501
        """Movie - a model defined in Swagger

        :param id: The id of this Movie.  # noqa: E501
        :type id: int
        :param name: The name of this Movie.  # noqa: E501
        :type name: str
        :param language: The language of this Movie.  # noqa: E501
        :type language: str
        :param outline: The outline of this Movie.  # noqa: E501
        :type outline: str
        :param age_rating: The age_rating of this Movie.  # noqa: E501
        :type age_rating: str
        :param duration_mins: The duration_mins of this Movie.  # noqa: E501
        :type duration_mins: int
        :param genres: The genres of this Movie.  # noqa: E501
        :type genres: List[str]
        :param imdb_rating: The imdb_rating of this Movie.  # noqa: E501
        :type imdb_rating: float
        :param director: The director of this Movie.  # noqa: E501
        :type director: str
        :param thumbnail: The thumbnail of this Movie.  # noqa: E501
        :type thumbnail: str
        :param cast: The cast of this Movie.  # noqa: E501
        :type cast: List[str]
        :param release_date: The release_date of this Movie.  # noqa: E501
        :type release_date: int
        :param distributor: The distributor of this Movie.  # noqa: E501
        :type distributor: str
        :param show_times: The show_times of this Movie.  # noqa: E501
        :type show_times: List[int]
        """
        self.swagger_types = {
            'id': int,
            'name': str,
            'language': str,
            'outline': str,
            'age_rating': str,
            'duration_mins': int,
            'genres': List[str],
            'imdb_rating': float,
            'director': str,
            'thumbnail': str,
            'cast': List[str],
            'release_date': int,
            'distributor': str,
            'show_times': List[int]
        }

        self.attribute_map = {
            'id': 'id',
            'name': 'name',
            'language': 'language',
            'outline': 'outline',
            'age_rating': 'age_rating',
            'duration_mins': 'duration_mins',
            'genres': 'genres',
            'imdb_rating': 'imdb_rating',
            'director': 'director',
            'thumbnail': 'thumbnail',
            'cast': 'cast',
            'release_date': 'release_date',
            'distributor': 'distributor',
            'show_times': 'show_times'
        }

        self._id = id
        self._name = name
        self._language = language
        self._outline = outline
        self._age_rating = age_rating
        self._duration_mins = duration_mins
        self._genres = genres
        self._imdb_rating = imdb_rating
        self._director = director
        self._thumbnail = thumbnail
        self._cast = cast
        self._release_date = release_date
        self._distributor = distributor
        self._show_times = show_times

    @classmethod
    def from_dict(cls, dikt) -> 'Movie':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Movie of this Movie.  # noqa: E501
        :rtype: Movie
        """
        return util.deserialize_model(dikt, cls)

    @property
    def id(self) -> int:
        """Gets the id of this Movie.


        :return: The id of this Movie.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this Movie.


        :param id: The id of this Movie.
        :type id: int
        """

        self._id = id

    @property
    def name(self) -> str:
        """Gets the name of this Movie.


        :return: The name of this Movie.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this Movie.


        :param name: The name of this Movie.
        :type name: str
        """

        self._name = name

    @property
    def language(self) -> str:
        """Gets the language of this Movie.


        :return: The language of this Movie.
        :rtype: str
        """
        return self._language

    @language.setter
    def language(self, language: str):
        """Sets the language of this Movie.


        :param language: The language of this Movie.
        :type language: str
        """

        self._language = language

    @property
    def outline(self) -> str:
        """Gets the outline of this Movie.


        :return: The outline of this Movie.
        :rtype: str
        """
        return self._outline

    @outline.setter
    def outline(self, outline: str):
        """Sets the outline of this Movie.


        :param outline: The outline of this Movie.
        :type outline: str
        """

        self._outline = outline

    @property
    def age_rating(self) -> str:
        """Gets the age_rating of this Movie.


        :return: The age_rating of this Movie.
        :rtype: str
        """
        return self._age_rating

    @age_rating.setter
    def age_rating(self, age_rating: str):
        """Sets the age_rating of this Movie.


        :param age_rating: The age_rating of this Movie.
        :type age_rating: str
        """

        self._age_rating = age_rating

    @property
    def duration_mins(self) -> int:
        """Gets the duration_mins of this Movie.


        :return: The duration_mins of this Movie.
        :rtype: int
        """
        return self._duration_mins

    @duration_mins.setter
    def duration_mins(self, duration_mins: int):
        """Sets the duration_mins of this Movie.


        :param duration_mins: The duration_mins of this Movie.
        :type duration_mins: int
        """

        self._duration_mins = duration_mins

    @property
    def genres(self) -> List[str]:
        """Gets the genres of this Movie.


        :return: The genres of this Movie.
        :rtype: List[str]
        """
        return self._genres

    @genres.setter
    def genres(self, genres: List[str]):
        """Sets the genres of this Movie.


        :param genres: The genres of this Movie.
        :type genres: List[str]
        """

        self._genres = genres

    @property
    def imdb_rating(self) -> float:
        """Gets the imdb_rating of this Movie.


        :return: The imdb_rating of this Movie.
        :rtype: float
        """
        return self._imdb_rating

    @imdb_rating.setter
    def imdb_rating(self, imdb_rating: float):
        """Sets the imdb_rating of this Movie.


        :param imdb_rating: The imdb_rating of this Movie.
        :type imdb_rating: float
        """

        self._imdb_rating = imdb_rating

    @property
    def director(self) -> str:
        """Gets the director of this Movie.


        :return: The director of this Movie.
        :rtype: str
        """
        return self._director

    @director.setter
    def director(self, director: str):
        """Sets the director of this Movie.


        :param director: The director of this Movie.
        :type director: str
        """

        self._director = director

    @property
    def thumbnail(self) -> str:
        """Gets the thumbnail of this Movie.


        :return: The thumbnail of this Movie.
        :rtype: str
        """
        return self._thumbnail

    @thumbnail.setter
    def thumbnail(self, thumbnail: str):
        """Sets the thumbnail of this Movie.


        :param thumbnail: The thumbnail of this Movie.
        :type thumbnail: str
        """

        self._thumbnail = thumbnail

    @property
    def cast(self) -> List[str]:
        """Gets the cast of this Movie.


        :return: The cast of this Movie.
        :rtype: List[str]
        """
        return self._cast

    @cast.setter
    def cast(self, cast: List[str]):
        """Sets the cast of this Movie.


        :param cast: The cast of this Movie.
        :type cast: List[str]
        """

        self._cast = cast

    @property
    def release_date(self) -> int:
        """Gets the release_date of this Movie.


        :return: The release_date of this Movie.
        :rtype: int
        """
        return self._release_date

    @release_date.setter
    def release_date(self, release_date: int):
        """Sets the release_date of this Movie.


        :param release_date: The release_date of this Movie.
        :type release_date: int
        """

        self._release_date = release_date

    @property
    def distributor(self) -> str:
        """Gets the distributor of this Movie.


        :return: The distributor of this Movie.
        :rtype: str
        """
        return self._distributor

    @distributor.setter
    def distributor(self, distributor: str):
        """Sets the distributor of this Movie.


        :param distributor: The distributor of this Movie.
        :type distributor: str
        """

        self._distributor = distributor

    @property
    def show_times(self) -> List[int]:
        """Gets the show_times of this Movie.


        :return: The show_times of this Movie.
        :rtype: List[int]
        """
        return self._show_times

    @show_times.setter
    def show_times(self, show_times: List[int]):
        """Sets the show_times of this Movie.


        :param show_times: The show_times of this Movie.
        :type show_times: List[int]
        """

        self._show_times = show_times
