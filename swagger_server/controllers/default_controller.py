import connexion
import six
import psycopg2

from collections import OrderedDict

from swagger_server.models.cinema_detail import CinemaDetail  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server import util


hostname = 'api.rella.im'
username = 'lms'
password = 'lms'
database = 'lms'

# Simple routine to run a query on a database and print the results:
def do_query(sql) :
    conn = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    cur = conn.cursor()
    cur.execute(sql)
    ans = cur.fetchall()
    conn.close()

    return ans

def get_cinemas(latitude, longitude):
    cur = do_query('select *, distance(latitude, longitude, %f, %f) from cinemas order by distance(latitude, longitude, %f, %f) limit 10;' % (latitude, longitude, latitude, longitude))
    ans = []
    for x in cur:
        ans.append({
            "id": x[0],
            "name": x[1],
            "thumbnail": x[2] or 'https://insideretail.asia/wp-content/uploads/2015/04/Vivo-City-official-415.jpg',
            "latitude": x[3],
            "longitude": x[4],
            "phone": x[5],
            "address": x[6],
            'rating': x[7],
            'distance_km': x[8],
            })
    return ans

def get_movies(id):
    sql = 'select * from cinema_movie cm left join movies m on m.id = cm.movie_id where cinema_id =%d and cm.show_time > extract(epoch from now()) and cm.show_time < (floor((extract(epoch from now()) + 8*3600) / (24*3600)) * (24*3600) + 24*3600 - 8*3600) order by show_time' % id
    movies_by_id = OrderedDict()
    visited = {}
    cur = do_query(sql)
    ans = []
    for x in cur:
        if (x[2], x[4]) not in visited:
            visited[(x[2], x[4])] = True
            movies_by_id.setdefault(x[2], [])
            movies_by_id[x[2]].append(x)
    for k, v in movies_by_id.items():
        show_times = []
        for t in v:
            show_times.append(t[4])

        ans.append({
            'id': k,
            'show_times': show_times,
            'name': v[0][7],
            'languages': v[0][8],
            'outline': v[0][9],
            'age_rating': v[0][10],
            'duration_mins': v[0][11],
            'genres': v[0][12],
            'imdb_rating': v[0][13],
            'director': v[0][14],
            'thumbnail': v[0][15] or 'https://insideretail.asia/wp-content/uploads/2015/04/Vivo-City-official-415.jpg',
            'cast': v[0][16],
            'release_date': v[0][17],
            'distributor': v[0][18],
            })
    return ans

def cinemas_get(country, city, latitude, longitude):  # noqa: E501
    """cinemas_get

     # noqa: E501

    :param country: 
    :type country: str
    :param city: 
    :type city: str
    :param latitude: 
    :type latitude: float
    :param longitude: 
    :type longitude: float

    :rtype: InlineResponse200
    """
    ans = {}
    ans['cinemas'] = get_cinemas(latitude, longitude)
    return ans


def cinemas_id_get(id):  # noqa: E501
    """cinemas_id_get

     # noqa: E501

    :param id: 
    :type id: int

    :rtype: CinemaDetail
    """
    ans = {}

    cur = do_query('select * from cinemas where id=%d;' % id)
    if len(cur) != 1:
        return 'Not found', 404, ''
    x = cur[0]

    ans['id'] = x[0]
    ans['name'] = x[1]
    ans['thumbnail'] = x[2] or 'https://insideretail.asia/wp-content/uploads/2015/04/Vivo-City-official-415.jpg'
    ans['latitude'] = x[3]
    ans['longitude'] = x[4]
    ans['phone'] = x[5]
    ans['address'] = x[6]
    ans['rating'] = x[7]


    ans['movies'] = get_movies(id)

    return ans
